import '../css/MyAccount.css';

import QuickAccess from "./QuickAccess";

import { BrowserRouter as Router, Link } from "react-router-dom";

function Home() {
    return (
      <div className="home">
       
        <h2>Bienvenue Sorcière des Mots</h2>
        
        <QuickAccess />
        <br/><br/>

        {/* chaque lien sera complété par un élément de la base de données dans laquelle se trouvent les textes afin de générer une page
        correspondant au projet demandé, il faudra donc éditer la génération de lien (bouche for pour générer les liens
        et mettre les titres), voir si c'est possible mais normalement oui.) */}
        <div id="mainHomeContent">

          <div>
            <h3> Grimoire en cours :</h3>
            <ul>
              <Link to="/textEditor"><li><i>La Chimère Rapiécée</i></li></Link>
              <Link to="/textEditor"><li><i>La sorcière à l'orée du village</i></li></Link>
              <Link to="/textEditor"><li><i>La chasseuse de malchance</i></li></Link>     
            </ul>
          </div>

          <div>
            <h3> Notes en vadrouille:</h3>
            <ul>           
              <Link to="/notes"><li>Notes pour <i>La Chimère Rapiécée</i></li></Link>
              <Link to="/notes"><li>Notes pour <i>La sorcière à l'orée du village</i></li></Link>
              <Link to="/notes"><li>Notes pour <i>La chasseuse de malchance</i></li></Link>     
            </ul>
          </div>
          
          <div>
            <h3> Sources et Inspiration :</h3>
            <ul>
              <Link to="/sources"><li>Plantes vénéneuses et Plantes Bienfaitrices</li></Link>
              <Link to="/sources"><li>La magie des druides</li></Link>
              <Link to="/sources"><li>Enigmes policières inexpliquées du XIXème siècle</li></Link>
            </ul>
          </div>
        </div>
        
      </div>
    );
  }
  
  export default Home;