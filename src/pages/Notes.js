import '../css/Config.css';

function Config() {
    return (
      <div className="Config">

        <div className='contentBox'>
        
          <h2>Bienvenue à l'entrée du Labyrinthe des souvenirs éthérés.</h2>

           {/* chaque lien sera complété par un élément de la base de données dans laquelle se trouvent les textes afin de générer une page
        correspondant au projet demandé, il faudra donc éditer la génération de lien (boucle for pour générer les liens
        et mettre les titres), voir si c'est possible mais normalement oui.) */}
          <p>Ici errent les fantômes des mondes oubliés...</p>
          <p>Et des univers qui peut-être jamais ne pourront exister.</p>
          <p>Contemplez les pages fanées d'une imagination singulièrement dérangée.</p>

        </div>
       
      </div>
    );
  }
  
  export default Config;