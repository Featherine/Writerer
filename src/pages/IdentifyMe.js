import '../css/IdentifyMe.css';

import { BrowserRouter as Router, Link } from "react-router-dom";

function IdentifyMe() {
    return (
      <div className="identifyPage">

        <div className="appPresentation">
          <p>Bienvenue sur <em>Oniro Scrib</em>, une application dédiée à l'écriture.</p>
          <p>Grâce à cette application vous aurez accès à une interface de travail qui vous permettra d'accéder facilement à vos différents documents
          nécessaires à la rédaction de vos histoires sans que vous n'ayez à jongler entre différentes fenêtres. Il vous sera également possible de 
          créer des fiches de personnages, écrire des notes, dessiner le plan de votre histoire et même créer une bibliothèque de sources accessible
          directement dans l'interface.</p>
          <p>Enfin, <em>Oniro Scrib</em> vous permet également de faire du versioning afin de vous permettre de retourner facilement à un état antérieur de votre projet,
          que ce soit pour créer des versions alternatives ou tout simplement retravailler votre texte.</p>
        </div>

        <div className="identifyMe">

          <div id="memberLog">
            <h2>Déjà un compte ?</h2>
            <form>
              <li>
                <label for="mail">Votre adresse mail ou votre identifiant :</label> <br/>
                <input type="mail" id="mail" name="mail" size="40" required></input>
              </li>

              <li>
                <label for="password">Votre mot de passe :</label> <br/>
                <input type="password" id="password" name="password" size="40" required></input>
              </li>

              <li>
                <button>Valider</button>
              </li>
            </form>

            <p><Link to="/resetPassword">Mot de passe oublié ?</Link></p>

          </div>



          <div id="NewMember">
            
            <h2>M'inscrire</h2>

            <p><Link to="/subscribe">Cliquez ici pour vous inscrire</Link></p>

          </div>
          
        </div>
      </div>
    );
  }
  
  export default IdentifyMe;