import '../css/NewPassword.css';

import { BrowserRouter as Router, Link } from "react-router-dom";

function NewPassword() {
    return (
      <div className="resetPassword">

      <h2>Réinitialisation de votre mot de passe</h2>

        <form action = "" method= "post">
            <li>
                <label for="mail">Veuillez entrer votre adresse mail svp :</label> <br/>
                <input type="mail" id="mail" name="mail" size="40" required></input>
             </li>
            <li>
                <button>Envoyer</button>
            </li>
        </form>
        
      </div>
    );
  }
  
  export default NewPassword;