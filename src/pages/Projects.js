import { BrowserRouter as Router, Link } from "react-router-dom";

import '../css/Projects.css';

function Projects() {
    return (
      <div className="Project">

        <div className="contentBox">

        <Link to="/new_project"><h2>Créer un nouveau projet</h2></Link>

          <h2>projets en cours</h2>

        {/* chaque lien sera complété par un élément de la base de données dans laquelle se trouvent les textes afin de générer une page
        correspondant au projet demandé, il faudra donc éditer la génération de lien (bouche for pour générer les liens
        et mettre les titres), voir si c'est possible mais normalement oui.) */}

          <ul>
            <Link to="/textEditor"><li>La Chimère Rapiécée</li></Link>
            <Link to="/textEditor"><li>La sorcière à l'orée du village</li></Link>
            <Link to="/textEditor"><li>La chasseuse de malchance</li></Link>     
          </ul>
          <br/><br/>

          <h2>projets achevés</h2>

          <ul>
            <Link to="/textEditor"><li>La femme en blanc</li></Link>
            <Link to="/textEditor"><li>L'île de la sorcière</li></Link>
            <Link to="/textEditor"><li>Le mystère de l'arbre creux</li></Link>
            <Link to="/textEditor"><li>Le pendu qui aimait les pendules</li></Link>
            <Link to="/textEditor"><li>Contraint de Mariage</li></Link>
          </ul>

        </div>

      </div>
    );
  }
  
  export default Projects;