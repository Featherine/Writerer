import '../css/TextEditor.css';


import { useMemo, useState } from 'react';
import { useCallback } from "react";
import { createEditor } from 'slate';
import { Slate, Editable, withReact } from 'slate-react';
import { Editor, Transforms, Text, Node } from 'slate';

import '../js/saveFile';

import SaveIcon from "../pictures/icones/text_editor_icons/diskette-save2-svgrepo-com.svg";
import BoldIcon from "../pictures/icones/text_editor_icons/bold-svgrepo-com.svg";
import ItalicIcon from "../pictures/icones/text_editor_icons/italic-svgrepo-com.svg";
import UnderlineIcon from "../pictures/icones/text_editor_icons/underline3-svgrepo-com.svg";




const CustomEditor = {

  isBoldMarkActive(editor){
    const[match] = Editor.nodes(editor, {
      match: n => n.bold ===true,
      universal: true,
    })

    return !!match

  },

  isItalicMarkActive(editor){
    const[match] = Editor.nodes(editor, {
      match: n => n.italic ===true,
      universal: true,
    })

    return !!match

  },

  isUnderlineMarkActive(editor){
    const[match] = Editor.nodes(editor, {
      match: n => n.underline ===true,
      universal: true,
    })

    return !!match

  },

  isStrikethroughMarkActive(editor){
    const[match] = Editor.nodes(editor, {
      match: n => n.strikethrough ===true,
      universal: true,
    })
  
  return !!match
  },



  toggleBoldMark(editor){
    const isActive = CustomEditor.isBoldMarkActive(editor)
    Transforms.setNodes(
      editor,
      {bold : isActive ? null : true},
      {match: n => Text.isText(n), split :true}
    )
  },

  toggleItalicMark(editor){
    const isActive = CustomEditor.isItalicMarkActive(editor)
    Transforms.setNodes(
      editor,
      {italic : isActive ? null : true},
      {match: n => Text.isText(n), split :true}
    )
  },

  toggleUnderlineMark(editor){
    const isActive = CustomEditor.isUnderlineMarkActive(editor)
    Transforms.setNodes(
      editor,
      {underline : isActive ? null : true},
      {match: n => Text.isText(n), split :true}
    )
  },

  toggleStrikethroughMark(editor){
    const isActive = CustomEditor.isStrikethroughMarkActive(editor)
    Transforms.setNodes(
      editor,
      {strikethrough : isActive ? null : true},
      {match: n => Text.isText(n), split : true}
    )
  }

}


function TextEditor() {


  const [editor] = useState(() => withReact(createEditor()));

  const initialValue= useMemo( 
                      () => 
                        JSON.parse(localStorage.getItem('content')) || [
                        {
                          type: 'paragraph',
                          children: [{text: 'A line of text in the paragraph blabla'}],
                        },
                      ],
                      []
                    )


  const renderElement = useCallback(props => {
    switch (props.element.type){
      case 'code':
        return <CodeElement{...props} />
        default:
          return <DefaultElement {...props} />

    }
  }, [])

  const renderLeaf = useCallback(props => {
    return <Leaf {...props} />
  }, [])

    return (

          <Slate editor={editor}
           value={initialValue}
           onChange = {value => {
            const isAstChange = editor.operations.some(
              op=> 'set_selection' !== op.type
            )
            if (isAstChange) {
              const content = JSON.stringify(value)
              localStorage.setItem('content', content)
            }
           }}
            >

            <div className="textEditor">

              <ul className='editorToolbar'>

                <li>
                  <div className='iconBox'>
                    <img src={SaveIcon} alt='save_icon' id="save" value="Save note" className='saveIcon textEditorIcon'/>
                  </div>
                </li>

                <li>
                  <div onMouseDown={event => {
                    event.preventDefault()
                    CustomEditor.toggleBoldMark(editor)
                  }} className="iconBox">
                    <img src={BoldIcon} alt='bold_icon'className='boldIcon textEditorIcon' />
                  </div>
                </li>

                <li>
                  <div onMouseDown={event => {
                    event.preventDefault()
                    CustomEditor.toggleItalicMark(editor)
                  }} className='iconBox'>
                    <img src={ItalicIcon} alt='italic_icon' className='italicIcon textEditorIcon'/>
                  </div>
                </li>

                <li>
                  <div onMouseDown={event => {
                    event.preventDefault()
                    CustomEditor.toggleUnderlineMark(editor)
                  }} className='iconBox'>
                    <img src={UnderlineIcon} alt="underline_icon" className='underlineIcon textEditorIcon'/>
                  </div>
                </li>

                {/* <li>
                  <div onMouseDown={event => {
                    event.preventDefault()
                    CustomEditor.toggleStrikethroughMark(editor)
                  }}>
                    <p>strikethrough</p>
                  </div>
                </li> */}

              </ul>

              <Editable
                style={{'backgroundColor' : "#fff",
                        'width': '70%',
                        'height': '700px',
                        'border': 'solid #000 1px',
                        'borderRadius': '10px',
                        }}

                renderElement={renderElement}
                renderLeaf={renderLeaf}

                onKeyDown={event => { 

                  if (!event.ctrlKey){
                    return
                  }

                  switch(event.key){
                    case '`':{
                      event.preventDefault()

                      CustomEditor.toggleCodeBlock(editor)
                      break
                    }

                    case 's': {
                      event.preventDefault()
                      console.log("saved")
                      break
                    }

                    case 'b': {
                      event.preventDefault()
                      CustomEditor.toggleBoldMark(editor)
                      break
                    } 

                    case 'i': {
                      event.preventDefault()
                      CustomEditor.toggleItalicMark(editor)       
                      break
                    }    
                    
            
                    case '&': {
                      event.preventDefault()
                      editor.insertText('et')
                    }
                  }
                }}
              />

            </div>
          </Slate>

    );
  }


  const Leaf = props => {
    return (
      <span 
      {...props.attributes}
      style={{ fontWeight: props.leaf.bold ? 'bold' : 'normal',
              fontStyle: props.leaf.italic ? 'italic': 'normal',
              textDecoration: props.leaf.underline ? 'underline' : 'none',
              // textDecorationLine: props.leaf.strikethrough ? 'strikethrough' : 'none'
            }}
      >
        {props.children}
      </span>
    )
  }

  const CodeElement = props => {
    return (
      <pre {...props.attributes}>
         <code>{props.children}</code>
      </pre>
    )
  }
  
  const DefaultElement = props => {
    return <p {...props.attributes}>{props.children}</p>
  }


  
  export default TextEditor;