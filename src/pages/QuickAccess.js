import '../css/QuickAccess.css';

import newProjectIcon from "../pictures/newProjectIcon.png";
import newNotesIcon from "../pictures/newNotesIcon.png";
import newSourceIcon from "../pictures/newSourceIcon.png";

import { BrowserRouter as Router, Link } from "react-router-dom";

function QuickAccess() {
    return (
      <div className="quickAccess">
        
      <ul>
      {/* mettre une icone devant chaque catégorie */}
      <Link to="/textEditor"><li><img src={newProjectIcon} alt="nope" id="newProjectIcon" className="QuickAccessIcon"/> Nouveau Projet</li></Link>
      <Link to="/notes"><li><img src={newNotesIcon} alt="nope" id="newNotesIcon" className="QuickAccessIcon"/>Nouvelle Notes</li></Link>
      <Link to="/sources"><li><img src={newSourceIcon} alt="nope" id="newSourcesIcon" className="QuickAccessIcon"/>Ajouter Source</li></Link>
      </ul>
       
      </div>
    );
  }
  
  export default QuickAccess;




