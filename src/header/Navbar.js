import { BrowserRouter as Router, Link } from "react-router-dom"; 
/* Même si le terme Routeur n'est pas utilisé ici, il faut le conserver pour bien faire fonctionner le routeur.
Je suppose que c'est pour que le fichier garde en mémoire le fait que <Navbar /> est dans la balise Routeur sur App.js (?)*/

function Navbar() {
    return (
      <div className="Navbar">

        <ul>
          <Link to="/projects"> <li>Projets</li></Link>
          <Link to="/notes"><li>Notes</li></Link>
          <Link to="/sources"><li>Sources</li></Link>
          <Link to="/other"><li>Autres</li></Link>  
        </ul>

      </div>
    );
  }
  
  export default Navbar;
