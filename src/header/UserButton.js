import '../css/UserButton.css';

import { BrowserRouter as Router, Link } from "react-router-dom"; 

function UserButton() {
    return (
      <div className="buttonPage">
       
          <Link to="/connection" className="connectButton">Connexion</Link>
        
      </div>
    );
  }
  
  export default UserButton;