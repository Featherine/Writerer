import '../css/MenuButton.css';

import {useState} from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom"; 





function MenuButton() {

  const [isActive, setisActive] = useState(false);

  const handleClick = () => {
    setisActive(current => !current);
  }

  return (
    <div className="MenuButton">

      <ul className='menuButtonBox'>

        <li className='PictureBox'>
          {/* <p><img src={MenuPicture} alt="menu button" id="headerMenuButton" className="Button" onClick={handleClick}/></p> */}
          <div id="headerMenuButton" className="Button" onClick={handleClick}>
            <span>
              <div className='menuSquare'></div><div className='menuSquare'></div><div className='menuSquare'></div>
            </span>
            <span>
              <div className='menuSquare'></div><div className='menuSquare'></div><div className='menuSquare'></div>
            </span>
            <span>
              <div className='menuSquare'></div><div className='menuSquare'></div><div className='menuSquare'></div>
            </span>
          </div>
        </li>

        <div className='backgroundTab' style={{
              display: isActive? 'flex': 'none',
            }}>
          <Link to="/projects" onClick={handleClick}>
            <li>Projets</li>
          </Link>

          <Link to="/notes" onClick={handleClick}>
            <li>Notes</li>
          </Link>

          <Link to="/sources" onClick={handleClick}>
            <li>Sources</li>
          </Link>
              
          <Link to="/other" onClick={handleClick}>
            <li>Autres</li>
          </Link>  
        </div>
      </ul>
        
    </div>
  );

}

export default MenuButton;