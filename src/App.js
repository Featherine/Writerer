import './css/fonts.css';
import './css/App.css';
// pages pour le header
import Header from './header/Header';
import UserButton from './header/UserButton';
import Navbar from './header/Navbar';
import MenuButton from './header/MenuButton';

// pages de contenu
import MyAccount from './pages/MyAccount';
import IdentifyMe from './pages/IdentifyMe';
import Subscribe from './pages/Subscribe';
import NewPassword from './pages/NewPassword';
import Projects from './pages/Projects';
import NewProject from './pages/TextEditor';
import TextEditor from './pages/TextEditor';
import Notes from './pages/Notes';
import Sources from './pages/Sources';
import Other from './pages/Other';
import Error404 from './pages/Error404';

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <header className="headerContent">
           <div id='headerButton'>

            <MenuButton />
            <UserButton />
            
          </div>          
          {/* <Navbar /> */}
        </header>

        <main>
          <Routes>

            <Route path="/" element={<TextEditor />} />
            <Route path="/connection" element={<IdentifyMe />} />

            <Route path="/subscribe" element={<Subscribe />} />
            <Route path="/resetPassword" element={<NewPassword />} />
            <Route path="/myAccount" element={<MyAccount />} />

            <Route path="/projects" element={<Projects />} />
            <Route path="/new_project" element={<NewProject />} />
            <Route path="/textEditor" element={<TextEditor />} />
            <Route path="/notes" element={<Notes />} />
            <Route path="/sources" element={<Sources />} />
            <Route path="/other" element={<Other />} />

            <Route path='*' element={<Error404 />} />

          </Routes>          
        </main>

      </Router>

    </div>
  );
}

export default App;
